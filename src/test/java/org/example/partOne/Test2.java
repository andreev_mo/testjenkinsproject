package org.example.partOne;

import com.codeborne.selenide.Condition;
import org.example.BaseTest;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.$x;

public class Test2 extends BaseTest {

    @Test
    void test22() {
        $x("//button[@aria-label='Перевод сайтов']").shouldBe(Condition.exist);
    }
}
