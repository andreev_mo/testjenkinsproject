package org.example.partTwo;

import com.codeborne.selenide.Condition;
import org.example.BaseTest;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.$x;

class Test4 extends BaseTest {

    @Test
    void test44() {
        $x("//*[@aria-label='Перевод текста на изображениях']").shouldBe(Condition.exist);
    }

}