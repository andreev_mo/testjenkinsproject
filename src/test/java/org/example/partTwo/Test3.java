package org.example.partTwo;

import com.codeborne.selenide.Condition;
import org.example.BaseTest;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.$x;

public class Test3 extends BaseTest {

    @Test
    void test33() {
        $x("//button[@aria-label='Перевод документов']").shouldBe(Condition.exist);
    }
}
